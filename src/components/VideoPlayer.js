import React from "react";
import ReactPlayer from "react-player";

class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.handleVideoReady = this.handleVideoReady.bind(this);
    this.handleVideoEnded = this.handleVideoEnded.bind(this);
    this.handleVideoError = this.handleVideoError.bind(this);
    this.video = React.createRef();
  }

  handleVideoReady = () => {
    this.video.current.seekTo(0);
  };

  handleVideoEnded = () => {
    this.props.nextVideo();
  };

  handleVideoError = () => {
    this.props.nextVideo();
  };

  render() {
    return (
      <ReactPlayer
        ref={this.video}
        url={this.props.url}
        playing={this.props.play}
        controls={true}
        onEnded={this.handleVideoEnded}
        onError={this.handleVideoError}
        onReady={this.handleVideoReady}
        height="100%"
        width="100%"
      />
    );
  }
}

export default VideoPlayer;
