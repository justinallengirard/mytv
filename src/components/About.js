import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

export default function About(props) {
  return (
    <Modal centered show={props.showAbout} onHide={props.toggleAbout}>
      <Modal.Header>
        <Modal.Title>About</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col>
              <p>
                <b>Email: </b>JustinAllenGirard@gmail.com
              </p>
            </Col>
          </Row>
          <Row>
            <Col>
              <b>
                <u>Developed Using:</u>
              </b>
              <ul>
                <li>React</li>
                <li>React-Bootstrap</li>
                <li>React-Player</li>
                <li>Python</li>
                <li>PostgreSQL</li>
                <li>VSCode</li>
                <li>GitLab</li>
              </ul>
            </Col>
          </Row>
          <Row>
            <Col>
              <b>
                <u>AWS Services Used:</u>
              </b>
              <ul>
                <li>Route 53</li>
                <li>Amplify</li>
                <li>API Gateway</li>
                <li>Lambda</li>
                <li>RDS</li>
              </ul>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={props.toggleAbout}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
