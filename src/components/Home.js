import React from "react";
import Database from "../Database.js";
import Header from "./Header.js";
import Settings from "./Settings.js";
import About from "./About.js";
import ChannelSelector from "./ChannelSelector.js";
import VideoPlayer from "./VideoPlayer.js";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import Alert from "react-bootstrap/Alert";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentChannel: "",
      currentVideoTitle: "",
      currentVideoURL: "",
      currentVideoIndex: 0,
      channels: [],
      videos: [],
      showVideo: true,
      showChannelSelector: false,
      showSettings: false,
      showAbout: false,
      play: true,
      adminMode: false,
      showAlert: false,
      alertType: "",
      alertMessage: "",
    };

    this.Database = new Database();

    this.nextVideo = this.nextVideo.bind(this);
    this.getVideos = this.getVideos.bind(this);
    this.toggleSettings = this.toggleSettings.bind(this);
    this.toggleChannelSelector = this.toggleChannelSelector.bind(this);
    this.toggleAbout = this.toggleAbout.bind(this);
    this.refreshChannels = this.refreshChannels.bind(this);
    this.deleteVideo = this.deleteVideo.bind(this);
    this.deleteChannel = this.deleteChannel.bind(this);
    this.enableAdminMode = this.enableAdminMode.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }

  enableAdminMode = () => {
    this.setState({ adminMode: true });
  };

  toggleSettings = () => {
    this.setState({ showSettings: !this.state.showSettings });
  };

  toggleChannelSelector = () => {
    this.setState({ showChannelSelector: !this.state.showChannelSelector });
  };

  toggleAbout = () => {
    this.setState({ showAbout: !this.state.showAbout });
  };

  componentDidMount() {
    this.Database.getChannels((successful, channels) => {
      if (successful) {
        this.setState({ channels: channels });
        this.getVideos("Random");
      } else {
        this.showAlert("danger", "Failed to get channels!");
      }
    });
  }

  getVideos = (channel) => {
    this.setState({ play: false, showChannelSelector: false, currentChannel: channel }, () => {
      this.Database.getVideos(channel, (successful, videos) => {
        if (successful) {
          let i = videos.length - 1;
          for (; i > 0; i--) {
            const j = Math.floor(Math.random() * i);
            const temp = videos[i];
            videos[i] = videos[j];
            videos[j] = temp;
          }

          this.setState({
            videos: videos,
            currentVideoIndex: 0,
            currentVideoURL: videos[0].url,
            currentVideoTitle: videos[0].title,
            showVideo: true,
            play: true,
          });
        } else {
          this.setState({
            showVideo: false,
            currentVideoIndex: 0,
            currentVideoTitle: "",
            currentVideoURL: "",
            videos: [],
          });
        }
      });
    });
  };

  nextVideo = () => {
    let nextVideoIndex = this.state.currentVideoIndex + 1;
    if (nextVideoIndex >= this.state.videos.length) {
      nextVideoIndex = 0;
    }

    let nextVideoURL = this.state.videos[nextVideoIndex].url;
    let nextVideoTitle = this.state.videos[nextVideoIndex].title;
    this.setState({ currentVideoIndex: nextVideoIndex, currentVideoURL: nextVideoURL, currentVideoTitle: nextVideoTitle });
  };

  deleteVideo = () => {
    this.Database.deleteVideo(this.state.currentVideoURL, (successful) => {
      if (successful) {
        if (this.state.videos.length === 0) {
          this.getVideos("Random");
        } else {
          this.nextVideo();
          let newVideos = this.state.videos;
          newVideos.splice(this.state.currentVideoIndex, 1);
          this.setState({ videos: newVideos, currentVideoIndex: this.state.currentVideoIndex - 1 });
        }
        this.showAlert("success", "Video successfully deleted!");
      } else {
        this.showAlert("danger", "Failed to delete video!");
      }
    });
  };

  deleteChannel = () => {
    if (this.state.currentChannel === "Random") {
      alert("You cannot delete this channel.");
    }
    this.Database.deleteChannel(this.state.currentChannel, (successful) => {
      if (successful) {
        this.refreshChannels();
        this.showAlert("success", "Channel successfully deleted!");
      } else {
        this.showAlert("danger", "failed to delete channel!");
      }
    });
  };

  refreshChannels = () => {
    this.Database.getChannels((successful, channels) => {
      if (successful) {
        this.setState({ channels: channels });
        this.getVideos("Random");
      } else {
        this.showAlert("danger", "Failed to refresh channels!");
      }
    });
  };

  showAlert = (type, message) => {
    this.setState({ showAlert: true, alertMessage: message, alertType: type }, () => {
      window.setTimeout(() => {
        this.setState({ showAlert: false, alertMessage: "", alertType: "" });
      }, 2000);
    });
  };

  render() {
    return (
      <Container fluid className="vh-100 vw-100 d-flex flex-column" style={{ overflow: "hidden", margin: 0, padding: 0 }}>
        <Row style={{ flexGrow: 0, margin: 0, padding: 0 }}>
          <Col style={{ margin: 0, padding: 0 }}>
            <Alert
              variant={this.state.alertType}
              show={this.state.showAlert}
              style={{ width: 300, position: "fixed", zIndex: 4000, marginTop: 50, left: "50%", marginLeft: -150, textAlign: "center" }}
            >
              {this.state.alertMessage}
            </Alert>
            <Header
              currentChannel={this.state.currentChannel}
              currentVideoTitle={this.state.currentVideoTitle}
              toggleChannelSelector={this.toggleChannelSelector}
              toggleSettings={this.toggleSettings}
              toggleAbout={this.toggleAbout}
            />
            <ChannelSelector
              channels={this.state.channels}
              currentChannel={this.state.currentChannel}
              showChannelSelector={this.state.showChannelSelector}
              toggleChannelSelector={this.toggleChannelSelector}
              loadChannel={this.getVideos}
            />
            <Settings
              channels={this.state.channels}
              currentChannel={this.state.currentChannel}
              showSettings={this.state.showSettings}
              toggleSettings={this.toggleSettings}
              deleteVideo={this.deleteVideo}
              deleteChannel={this.deleteChannel}
              refreshChannels={this.refreshChannels}
              enableAdminMode={this.enableAdminMode}
              adminMode={this.state.adminMode}
              showAlert={this.showAlert}
            />
            <About showAbout={this.state.showAbout} toggleAbout={this.toggleAbout} />
          </Col>
        </Row>
        <Row style={{ flexGrow: 1, backgroundColor: "black", margin: 0, padding: 0 }}>
          <Col style={{ margin: 0, padding: 0 }}>
            {this.state.showVideo ? (
              <VideoPlayer url={this.state.currentVideoURL} nextVideo={this.nextVideo} play={this.state.play} />
            ) : (
              <Card bg="danger" text="white" style={{ width: "24rem", margin: "20px auto" }}>
                <Card.Body style={{ textAlign: "center" }}>There are no videos on this channel...</Card.Body>
              </Card>
            )}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Home;
