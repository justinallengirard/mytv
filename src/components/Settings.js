import React from "react";
import Database from "../Database.js";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { LockFill as LockIcon, UnlockFill as UnlockIcon } from "react-bootstrap-icons";

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newChannelName: "",
      newVideoTitle: "",
      newVideoURL: "",
      newVideoChannel: "",
      adminPassword: "",
    };

    this.Database = new Database();

    this.addChannel = this.addChannel.bind(this);
    this.addVideo = this.addVideo.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleAdminMode = this.handleAdminMode.bind(this);
  }

  addChannel = (event) => {
    this.Database.addChannel(this.state.newChannelName, (successful) => {
      if (successful) {
        this.props.refreshChannels();
        this.props.showAlert("success", "Channel successfully added!");
      } else {
        this.props.showAlert("danger", "Failed to add channel!");
      }

      this.setState({ newChannelName: "" });
    });
  };

  addVideo = (event) => {
    this.Database.addVideo(this.state.newVideoURL, this.state.newVideoTitle, this.state.newVideoChannel, (successful) => {
      if (successful) {
        this.props.showAlert("success", "Video successfully added!");
      } else {
        this.props.showAlert("danger", "Failed to add video!");
      }

      this.setState({ newVideoTitle: "", newVideoURL: "", newVideoChannel: "" });
    });
  };

  handleInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleAdminMode = (event) => {
    if (this.state.adminPassword === "rumHAM24#!") {
      this.props.enableAdminMode();
    }

    this.setState({ adminPassword: "" });
  };

  render() {
    return (
      <Modal centered show={this.props.showSettings} onHide={this.props.toggleSettings} dialogClassName="settings-modal">
        <Modal.Header>
          <Modal.Title>Settings</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Row>
              <Col>
                <div>
                  <Form.Label>
                    <b>Add New Channel</b>
                  </Form.Label>
                </div>
                <div style={{ marginBottom: 10 }}>
                  <Form.Control name="newChannelName" value={this.state.newChannelName} onChange={this.handleInputChange} placeholder="Channel Name" />
                </div>
                <div>
                  <Button
                    disabled={this.state.newChannelName === ""}
                    onClick={() => {
                      this.addChannel();
                    }}
                  >
                    Add Channel
                  </Button>
                </div>
                <div style={{ marginTop: 10 }}>
                  <Form.Label>
                    <b>Delete Current Channel</b>
                  </Form.Label>
                </div>
                <div>
                  <Button
                    variant="danger"
                    style={{ width: "100%" }}
                    onClick={() => {
                      this.props.deleteChannel();
                    }}
                    disabled={!this.props.adminMode}
                  >
                    Delete Current Channel {this.props.adminMode ? <UnlockIcon color="white" /> : <LockIcon color="white" />}
                  </Button>
                </div>
              </Col>
              <Col>
                <div>
                  <Form.Label>
                    <b>Add New Video</b>
                  </Form.Label>
                </div>
                <div style={{ marginBottom: 10 }}>
                  <Form.Control name="newVideoTitle" value={this.state.newVideoTitle} onChange={this.handleInputChange} placeholder="Video Title" />
                </div>
                <div style={{ marginBottom: 10 }}>
                  <Form.Control name="newVideoURL" value={this.state.newVideoURL} onChange={this.handleInputChange} placeholder="Video URL" />
                </div>
                <div style={{ marginBottom: 10 }}>
                  <Form.Control name="newVideoChannel" as="select" value={this.state.newVideoChannel} onChange={this.handleInputChange}>
                    <option value="" disabled hidden>
                      Video Channel
                    </option>
                    {this.props.channels.map((channel) => (
                      <option key={channel}>{channel}</option>
                    ))}
                  </Form.Control>
                </div>
                <div>
                  <Button
                    disabled={this.state.newVideoTitle === "" || this.state.newVideoURL === "" || this.state.newVideoChannel === ""}
                    onClick={() => {
                      this.addVideo();
                    }}
                  >
                    Add Video
                  </Button>
                </div>
                <div style={{ marginTop: 10 }}>
                  <Form.Label>
                    <b>Delete Current Video</b>
                  </Form.Label>
                </div>
                <div>
                  <Button
                    variant="danger"
                    style={{ width: "100%" }}
                    onClick={() => {
                      this.props.deleteVideo();
                    }}
                    disabled={!this.props.adminMode}
                  >
                    Delete Current Video {this.props.adminMode ? <UnlockIcon color="white" /> : <LockIcon color="white" />}
                  </Button>
                </div>
              </Col>
              <Col>
                <Row>
                  <Col>
                    <Form.Label>
                      <b>Admin Mode</b>
                    </Form.Label>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div style={{ marginBottom: 10 }}>
                      <Form.Control
                        name="adminPassword"
                        value={this.state.adminPassword}
                        onChange={this.handleInputChange}
                        placeholder="Password"
                        disabled={this.props.adminMode}
                      />
                    </div>
                    <div>
                      <Button
                        variant="primary"
                        style={{ width: "100%" }}
                        onClick={() => {
                          this.handleAdminMode();
                        }}
                        disabled={this.props.adminMode || this.state.adminPassword === ""}
                      >
                        Enable Admin Mode
                      </Button>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.toggleSettings}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default Settings;
