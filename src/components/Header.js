import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import { GearFill as SettingsIcon, InfoCircleFill as AboutIcon, Grid3x3GapFill as ChannelSelectorIcon } from "react-bootstrap-icons";

export default function Header(props) {
  return (
    <Navbar bg="primary" variant="dark" className="justify-content-between d-flex" style={{ textAlign: "center" }}>
      <Nav>
        <Nav.Item>
          <Button variant="link" onClick={props.toggleChannelSelector}>
            <ChannelSelectorIcon color="white" />
          </Button>
        </Nav.Item>
        <Nav.Item>
          <Navbar.Brand>MyTV</Navbar.Brand>
        </Nav.Item>
      </Nav>
      <div style={{ color: "white" }}>Current Channel: {props.currentChannel === "" ? "None" : props.currentChannel}</div>
      <div style={{ color: "white" }}>Current Video: {props.currentVideoTitle === "" ? "None" : props.currentVideoTitle}</div>
      <div>
        <Button variant="link" onClick={props.toggleSettings}>
          <SettingsIcon color="white" />
        </Button>
        <Button variant="link" onClick={props.toggleAbout}>
          <AboutIcon color="white" />
        </Button>
      </div>
    </Navbar>
  );
}
