import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import ListGroup from "react-bootstrap/ListGroup";

export default function ChannelSelector(props) {
  return (
    <Modal centered show={props.showChannelSelector} keyboard={false} onHide={props.toggleChannelSelector}>
      <Modal.Header>
        <Modal.Title>
          <p>Channel Selector </p>
          <p>
            <small>{props.channels.length} Channels</small>
          </p>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ListGroup variant="flush" style={{ maxHeight: 400, overflowY: "auto" }}>
          {props.channels.map((channel) =>
            props.currentChannel === channel ? (
              <ListGroup.Item active key={channel}>
                {channel}
              </ListGroup.Item>
            ) : (
              <ListGroup.Item
                key={channel}
                onClick={() => {
                  props.loadChannel(channel);
                }}
              >
                {channel}
              </ListGroup.Item>
            )
          )}
        </ListGroup>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={props.toggleChannelSelector}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
