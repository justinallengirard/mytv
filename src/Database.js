class Database {
  constructor() {
    this.url = "https://9ktjchbz3i.execute-api.us-east-2.amazonaws.com/dev/";
    this.headers = {
      "Content-Type": "application/json",
    };
    this.mode = "cors";
    this.credentials = "omit";
    this.httpPOST = "POST";
    this.httpGET = "GET";
  }

  addChannel(name, callback) {
    var parameters = {
      function: "ADD_CHANNEL",
      name: name,
    };

    fetch(new URL(this.url), {
      method: this.httpPOST,
      mode: this.mode,
      credentials: this.credentials,
      body: JSON.stringify(parameters),
      headers: this.headers,
    })
      .then((response) => response.json())
      .then(
        (response) => {
          callback(response.successful);
        },
        (error) => {
          callback(false);
        }
      );
  }

  addVideo(url, title, channel, callback) {
    var parameters = {
      function: "ADD_VIDEO",
      url: url,
      title: title,
      channel: channel,
    };

    fetch(new URL(this.url), {
      method: this.httpPOST,
      mode: this.mode,
      credentials: this.credentials,
      body: JSON.stringify(parameters),
      headers: this.headers,
    })
      .then((response) => response.json())
      .then(
        (response) => {
          callback(response.successful);
        },
        (error) => {
          callback(false);
        }
      );
  }

  deleteChannel(channel, callback) {
    var parameters = {
      function: "DELETE_CHANNEL",
      channel: channel,
    };
    fetch(new URL(this.url), {
      method: this.httpPOST,
      mode: this.mode,
      credentials: this.credentials,
      body: JSON.stringify(parameters),
      headers: this.headers,
    })
      .then((response) => response.json())
      .then(
        (response) => {
          callback(response.successful);
        },
        (error) => {
          callback(false);
        }
      );
  }

  deleteVideo(url, callback) {
    var parameters = {
      function: "DELETE_VIDEO",
      url: url,
    };

    fetch(new URL(this.url), {
      method: this.httpPOST,
      mode: this.mode,
      credentials: this.credentials,
      body: JSON.stringify(parameters),
      headers: this.headers,
    })
      .then((response) => response.json())
      .then(
        (response) => {
          callback(response.successful);
        },
        (error) => {
          callback(false);
        }
      );
  }

  getChannels(callback) {
    var url = new URL(this.url);
    url.searchParams.append("function", "GET_CHANNELS");

    fetch(url, {
      method: this.httpGET,
      mode: this.mode,
      credentials: this.credentials,
      body: null,
      headers: this.headers,
    })
      .then((response) => response.json())
      .then(
        (response) => {
          if (response.successful) {
            callback(true, response.data);
          } else {
            callback(false, "");
          }
        },
        (error) => {
          callback(false, error);
        }
      );
  }

  getVideos(channel, callback) {
    var url = new URL(this.url);
    url.searchParams.append("function", "GET_VIDEOS");
    url.searchParams.append("channel", channel);
    fetch(url, {
      method: this.httpGET,
      mode: this.mode,
      credentials: this.credentials,
      body: null,
      headers: this.headers,
    })
      .then((response) => response.json())
      .then(
        (response) => {
          if (response.successful) {
            callback(true, response.data);
          } else {
            callback(false, "");
          }
        },
        (error) => {
          callback(false, error);
        }
      );
  }
}

export default Database;
