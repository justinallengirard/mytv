import React from "react";
import "./styles/App.css";
import Home from "./components/Home.js";

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
